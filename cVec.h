#ifndef CVEC_H
#define CVEC_H

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/* internal macro function to return false on a matched condition */
#define _cVec_check(e)  \
{                       \
    if((e)) {           \
        return false;   \
    }                   \
}

/**
 * @brief Simpe vecotor representation.
 */
typedef struct {
    size_t cap;         /**< The capacity of the vector. */
    size_t elemSize;    /**< size of one element in byte. */
    size_t size;        /**< stored number of elements. */
    uint8_t* data;      /**< pointer to the allocated memory. */
} cVec_t;

/**
 * @brief Creates a vector.
 * @param[in] v pointer to the vector object.
 * @param[in] elemSize size of one element in byte. 
 * @return true on success.
 */
static bool cVec_new(cVec_t* v, size_t elemSize) {
    uint8_t* tmp = NULL;
    const size_t DEFAULT_CAPACITY = 8U;
    /* sanaty input check */
    _cVec_check((NULL == v) || (0 == elemSize));

    tmp = (uint8_t*)malloc(DEFAULT_CAPACITY * elemSize);
    _cVec_check(NULL == tmp);

    v->cap = DEFAULT_CAPACITY;
    v->elemSize = elemSize;
    v->size = 0U;
    v->data = tmp;
    return true;
}

/**
 * @brief Destroys the vector.
 * @param[in] v pointer to the vector object.
 * @return true on success.
 */
static bool cVec_delete(cVec_t* v) {
    /* sanaty input check */
    _cVec_check((NULL == v) || (NULL == v->data));

    v->cap = 0U;
    v->elemSize = 0U;
    v->size = 0U;
    free(v->data);
    return true;
}

/**
 * @brief Returns the element at a given index.
 * @param[in] v pointer to the vector object.
 * @param[in] i index of the element. 
 * @param[out] e pointer to store the element from the vector.
 * @return true on success.
 */
static inline bool cVec_get(cVec_t* v, size_t i, void* e) {
    /* sanaty input check */
    _cVec_check((NULL == v) 
                || (NULL == v->data) 
                || (NULL == e)
                || (0 == v->cap) 
                || (0 == v->elemSize)
                || (i > v->cap));
    return NULL != memcpy(e, (void*)&v->data[v->elemSize * i], v->elemSize);
}

/**
 * @brief Stores the element at a given index.
 * @param[in] v pointer to the vector object.
 * @param[in] i index of the element. 
 * @param[out] e pointer to store the element from the vector.
 * @return true on success.
 */
static inline bool cVec_set(cVec_t* v, size_t i, void* e) {
    /* sanaty input check */
    _cVec_check((NULL == v) 
                || (NULL == v->data) 
                || (NULL == e)
                || (0 == v->cap) 
                || (0 == v->elemSize)
                || (i > v->cap));
    return NULL != memcpy((void*)&v->data[v->elemSize * i], e, v->elemSize);
}

/**
 * @brief Returns the size of the vector.
 * @param[in] v pointer to the vector object.
 * @param[out] size pointer where the size of the vector will be stored.
 * @return true on success.
 */
static inline bool cVec_size(cVec_t* v, size_t* size) {
    /* sanaty input check */
    _cVec_check((NULL == v) 
                || (NULL == v->data) 
                || (0 == v->cap) 
                || (0 == v->elemSize)
                || (NULL == size));
    *size = v->size;
    return true;
}

/**
 * @brief Returns the capacity of the vector.
 * @param[in] v pointer to the vector object.
 * @param[out] capacity pointer where the size of the vector will be stored.
 * @return true on success.
 */
static inline bool cVec_capacity(cVec_t* v, size_t* capacity) {
    /* sanaty input check */
    _cVec_check((NULL == v) 
                || (NULL == v->data) 
                || (0 == v->cap) 
                || (0 == v->elemSize)
                || (NULL == capacity));
    *capacity = v->cap;
    return true;
}

/**
 * @brief Adds a element to the vectors end.
 * @param[in] v pointer to the vector object.
 * @param[out] e pointer to the element which is than stored in the vector.
 * @return true on success.
 */
static bool cVec_push(cVec_t* v, void* e) {
    uint8_t* tmp = NULL;
    /* sanaty input check */
    _cVec_check((NULL == v) 
                || (NULL == v->data) 
                || (NULL == e)
                || (0 == v->cap) 
                || (0 == v->elemSize));

    /* check if the vector data is full and the data needs to be extended */
    if(v->size >= v->cap) {
        tmp = (uint8_t*)realloc(v->data, v->cap * 2 * v->elemSize);
        _cVec_check(NULL == tmp);
        v->data = tmp;
        v->cap *= 2;
    }

    
    _cVec_check(false == cVec_set(v, v->size, e));
    v->size++;
    return true;
}

/**
 * @brief returns the last element and deletes it from the vector.
 * @param[in] v pointer to the vector object.
 * @param[out] e pointer to store the element from the vector. In case of a null pointer the value won't be returned.
 * @return true on success.
 */
static bool cVec_pop(cVec_t* v, void* e) {
    /* sanaty input check */
    _cVec_check((NULL == v) 
                || (NULL == v->data) 
                || (0 == v->size) 
                || (0 == v->cap) 
                || (0 == v->elemSize));
    v->size--;
    return (NULL == e) ? true : cVec_get(v, v->size, e);
}

#endif /* CVEC_H */
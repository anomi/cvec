#include "stdio.h"
#include "cVec.h"

typedef struct {
    int a;
    int b;
} dummy_t;

int main(int argc, char const *argv[])
{
    dummy_t in = {1, 2};
    dummy_t out = {0, 0};
    cVec_t v;
    size_t size;

    cVec_new(&v, sizeof(dummy_t));

    for(int i = 0; i < 16; i++)
    {
        in.a++;
        in.b++;
        cVec_push(&v, (void*)&in);
    }

    cVec_size(&v, &size);
    printf("v.size = %ld\n", size);

    cVec_get(&v, 0, (void*)&out);
    printf("v.get(): out.a = %d, out.b = %d\n", out.a, out.b);

    in.a = 99;
    in.b = 100;
    cVec_set(&v, 1, (void*)&in);

    while(true == cVec_pop(&v, (void*)&out))
        printf("cVec_pop(): out.a = %d, out.b = %d\n", out.a, out.b);

    cVec_delete(&v);

    return 0;
}
